# vim-spl

### Features
spl is a toy programming language used in the course Compiler Construction on the Radboud University.

This plugin provides syntax highlighting for `*.spl` files

### How to install
- Pathogen:

	`$ git clone gitlab.science.ru.nl/compilerconstruction/vim-spl.git ~/.vim/bundle/vim-spl.git`

- Vim pack (>8.0)

	Just clone the repo in the `~/.vim/pack/plugins/start` folder


### License
```
"THE BEER-WARE LICENSE" (Revision 42):
<mart@martlubbers.net> wrote this file. As long as you retain this notice you
can do whatever you want with this stuff. If we meet some day, and you think
this stuff is worth it, you can buy me a beer in return.
```
