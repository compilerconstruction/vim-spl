" Vim syntax file
" Language: SPL
" Maintainer: Mart
" Version: 1.0
" Latest Revision: 2016-05-31

if exists("b:current_syntax")
	finish
endif

" Keywords
syn keyword splConditionals if else
syn keyword splRepeats while
syn keyword splKeywords return
syn keyword splBuiltins print read isEmpty
syn keyword splFieldSelectors hd tl fst snd
syn keyword splBooleans True False

" Numbers and operators and strings
syn match splNumbers '[-]\?\d\+'
syn match splOperators '\([<>!=]=\|[<>+\\\-*%/=:]\|&&\|||\)'
syn match splStrings '"\(\\.\?"\|[^"]\)*"'

" Comments
syn keyword spTodo TODO FIXME XXX BUG NB NOTE contained containedin=splComment,splSingleComment
syn region splComment start="/\*" end="\*/" contains=@Spell
syn region splSingleComment start="//" end="$" contains=@Spell oneline display

" Functions
syn match splFunctions '[a-z\-A-Z_]\+\s*$\?(\@='

" types
syn keyword splTypes Int Bool Char Void
syn match splTypes '^\s\+\(\[.*\]\(\s*[a-zA-Z]\|->\|\s*{\)\@=\|(.*,.*)\)'

" Link it
hi def link splConditionals Conditional
hi def link splRepeats Repeat
hi def link splKeywords Keyword
hi def link splComment Comment
hi def link splSingleComment Comment
hi def link splNumbers Number
hi def link splBooleans Boolean
hi def link splOperators Operator
hi def link splStrings String
hi def link splFunctions Function
hi def link splBuiltins Function
hi def link splTypes Type
